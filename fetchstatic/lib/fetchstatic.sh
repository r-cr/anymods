fetchstatic_remote_scp()
{
    if [ "${any_remote_src_hashing}" = '1' ] ; then
        remote_name=$( hash_atom_name ${MY_P} )
    else
        remote_name=${MY_P}
    fi
    if [ "${any_local_src_hashing}" = '1' ] ; then
        local_name=$( hash_atom_name ${MY_P} )
        mkdir -p $(dirname ${local_name})
    else
        local_name=${MY_P}
    fi
    local_dir=${ANYSRCDIR}
    mkdir -p ${local_dir}

    for ext in ${any_fetch_arch_ext} ; do
        scp -q ${any_fetch_scp_string}${remote_name}${ext} ${local_dir}/${local_name}${ext} 2> /dev/null || nonfail
        if [ -f ${local_dir}/${local_name}${ext} ] ; then
            break
        fi
    done
}

fetch_remote_src()
{
    fetchstatic_remote_scp
}
