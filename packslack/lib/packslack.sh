
packslack_gen_control()
{
    fallback="$(pack_fallback_allowed)"

    # clean up
    pack_clean_tmp_subpackage "install/ TMPSLACK/"

    # parse variables with control information
    # info from variables has highest priority

    meta_inner_version=
    if test -n "${SLACK_INNER_VERSION}" ; then
        meta_inner_version="TMPSLACK/inner_version"
        printf "%s\n" "${SLACK_INNER_VERSION}" | pack_parse_subpackage_text "${meta_inner_version}" "${fallback}"
    fi
    meta_desc=
    set_up_desc=
    if test -n "${SLACK_DESCRIPTION}" ; then
        meta_desc="TMPSLACK/description"
        printf "%s\n" "${SLACK_DESCRIPTION}" | pack_parse_subpackage_text "${meta_desc}" "${fallback}"
    elif test -n "${DESCRIPTION}" ; then
        meta_desc_nonformat="TMPSLACK/description.nonformat"
        printf "%s\n" "${DESCRIPTION}" | pack_parse_subpackage_text "${meta_desc_nonformat}" "${fallback}"
    fi

    meta_arch=
    set_up_arch=
    if test -n "${SLACK_ARCHITECTURE}" ; then
        set_up_arch="${SLACK_ARCHITECTURE}"
    elif test -n "${PKG_ARCHITECTURE}" ; then
        set_up_arch="${PKG_ARCHITECTURE}"
    fi
    if test -n "${set_up_arch}" ; then
        meta_arch="TMPSLACK/arch"
        printf "%s\n" "${set_up_arch}" | pack_parse_subpackage_text "${meta_arch}" "${fallback}"
    fi

    # TODO: add depends later

    for name in $(pack_name_base) ; do
        binname="$(pack_name_from_shortname ${name})"
        bindir=$(pack_dir_for_runtime_entry "${binname}" "${fallback}" )
        if test ! -d "${bindir}" ; then
            continue
        fi

        file_desc=
        if test -n "${meta_desc}" ; then
            if test -s "${bindir}/${meta_desc}" ; then
                file_desc="${bindir}/${meta_desc}"
            fi
        elif test -n "${meta_desc_nonformat}" ; then
            if test -s "${bindir}/${meta_desc_nonformat}" ; then
                file_desc="${bindir}/TMPSLACK/description.format"
                cat "${bindir}/${meta_desc_nonformat}" | slack_format_description "${name}" > "${file_desc}"
                if test ! -s "${file_desc}" ; then
                    file_desc=
                fi
            fi
        else
            # find the package files, prepared beforehand
            source_desc=$(pack_metafile_for_runtime_entry "packslack/${name}" "slack-desc" "${fallback}")
            if test -z "${source_desc}" ; then
                source_desc=$(pack_metafile_for_runtime_entry "${name}" "slack-desc" "${fallback}")
            fi
            if test -s "${source_desc}" ; then
                file_desc="${source_desc}"
            fi
        fi
        if test -n "${file_desc}" ; then
            mkdir -p "${bindir}/install/"
            cp -f "${file_desc}" "${bindir}/install/slack-desc"
        fi

        # Final clean up
        rm -rf "${bindir}/TMPSLACK/" || nonfail

    done
}

packslack_cp_scripts()
{
    fallback="$(pack_fallback_allowed)"
    for name in $(pack_name_base) ; do
        binname="$(pack_name_from_shortname ${name})"
        bindir=$(pack_dir_for_runtime_entry "${binname}" "${fallback}" )
        if test ! -d "${bindir}" ; then
            continue
        fi
        mkdir -p "${bindir}/install/"

        found_postinst=
        # two checks: in specialized dir packslack/ and in general place
        for setup_script in doinst.sh postinst ; do
            binscript=$(pack_metafile_for_runtime_entry "packslack/${name}" "${setup_script}" "${fallback}")
            if test -z "${binscript}" ; then
                binscript=$(pack_metafile_for_runtime_entry "${name}" "${setup_script}" "${fallback}")
            fi
            if test -f "${binscript}" ; then
                found_postinst=1
                break
            fi
        done

        if test -n "${found_postinst}" ; then
            cp -f "${binscript}" "${bindir}/install/doinst.sh"
            chmod 555 "${bindir}/install/doinst.sh"
        else
            # check for archived script
            setup_script="doinst.sh.gz"
            binscript=$(pack_metafile_for_runtime_entry "packslack/${name}" "${setup_script}" "${fallback}")
            if test -z "${binscript}" ; then
                binscript=$(pack_metafile_for_runtime_entry "${name}" "${setup_script}" "${fallback}")
            fi
            if test -f "${binscript}" ; then
                zcat "${binscript}" > "${bindir}/install/doinst.sh"
                chmod 555 "${bindir}/install/doinst.sh"
                found_postinst=1
            fi
        fi

        if test -z "${found_postinst}" ; then
            rmdir "${bindir}/install/"
        fi

    done
}

pack_deploy_single_archive()
{
    single_archive="$1"
    fallback="$2"

    if test ! -f "${single_archive}" ; then
        awarn "no expected pack ${single_archive}"
        return 4
    fi
    alog "Deploy archive ${single_archive}"

    if test -n "${fallback}" ; then
        destdir="${D}"
    else
        # deploy each archive into separate dir, need to calculate dir name
        template_name="${single_archive##*/}"
        # remove file extension
        template_name="${template_name%.txz}"
        # remove inner version
        template_name="${template_name%-*}"
        # remove arch
        pack_full="${template_name%-*}"

        destdir="${IMGDIR}/${pack_full}"
    fi

    mkdir -p "${destdir}"
    tar xf "${single_archive}" -C "${destdir}"
    if test -d "${destdir}/install" ; then
        rm -rf "${destdir}/install"
    fi

    # for several archives of single pack only the last inner version will remain
    version_inner="$(pack_get_inner_version ${single_archive})"
    mkdir -p "${T}"
    printf "%s\n" "${version_inner}" > "${T}/inner_version"
}

# interfaces

pack_gen_meta()
{
    packslack_gen_control
    packslack_cp_scripts
}

pack_name_file()
{
    need_real_files="$1"
    if test -n "${need_real_files}" ; then
        if test -n "${any_pkg}" ; then
            pack_dir="$(cd ${any_pkg} ; pwd -P)"
        else
            pack_dir="${PACKDIR}"
        fi
        # assumption: do not use strict inner versions for real files.
        # instead check matching files with possible various inner versions.
        # if one needs real files + strict inner versions, he should call pack_name_file without the argument
        # and check the existence of files outside of the call
        # (since the file name is single entry and not a template of possible ones, that is straighforward)

        inner_version=
    else
        inner_version="$(slack_string_inner_version)"
    fi

    arch_default="$(slack_arch)"
    fallback="$(pack_fallback_allowed)"

    explicit_arch=
    if test -n "${SLACK_ARCHITECTURE}" ; then
        explicit_arch="${SLACK_ARCHITECTURE}"
    elif test -n "${PKG_ARCHITECTURE}" ; then
        explicit_arch="${PKG_ARCHITECTURE}"
    fi
    if test -n "${explicit_arch}" ; then
        meta_arch="TMPNAME/arch"
        pack_clean_tmp_subpackage "${meta_arch}"
        printf "%s\n" "${explicit_arch}" | pack_parse_subpackage_text "${meta_arch}" "${fallback}"
    fi

    flav_string=
    for fl_word in ${FLAVOUR} ; do
        flav_string="${flav_string}-${fl_word}"
    done
    if [ "${BP}" != "${P}" ] ; then
        entry_array="${BP}"
        flag_version_combined=1
    else
        entry_array="${BPN}"
        flag_version_combined=
    fi
    for entry in ${entry_array} ; do
        if test -n "${flag_version_combined}" ; then
            binname="${entry}"
            pack_try_name="$(depend_get_pack_name ${entry})"
        else
            binname="$(pack_name_from_shortname ${entry})"
            pack_try_name="${entry}"
        fi
        bindir=$(pack_dir_for_runtime_entry "${binname}" "${fallback}" )
        arch=

        if test -n "${explicit_arch}" ; then
            arch="$( cat "${bindir}/${meta_arch}" )"
            rm -rf "${bindir}/TMPNAME/"
        fi
        if test -z "${arch}" ; then
            arch="${arch_default}"
        fi

        if test -n "${flag_version_combined}" ; then
            pack_try_version="$(depend_get_pack_version ${entry})"
        else
            pack_try_version="${BPV}"
        fi
        pack_try_name="${pack_try_name}${flav_string}"

        if test -n "${need_real_files}" ; then
            # skip inner version on purpose - see comment at the beginning
            # expanding several files by * symbol without outer binary
            expansion="$( echo "${pack_dir}/${pack_try_name}-${pack_try_version}-${arch}"*".txz" )"

            # the problem here is the last position in string would be alhabetical: the maximum digit
            # but it should be the maximum version (11 versus 7, for example)
            # so need to find maximum explicitly
            pack_try=
            for candidate in ${expansion} ; do
                if arith_greater_then "${pack_try}" "${candidate}" ; then
                    pack_try="${candidate}"
                fi
            done
            if test -s "${pack_try}" ; then
                printf "%s\n" "${pack_try}"
            fi
        else
            pack_try="${pack_try_name}-${pack_try_version}-${arch}${inner_version}.txz"
            printf "%s\n" "${pack_try}"
        fi
    done
}

pack_create_archive()
{
    sourcedir="$1"
    archivefile="$2"

    alog "Creating binary package: slack archive ${archivefile}"
    cd ${sourcedir}
    ${UTILROOT} makepkg -l y -c n ${archivefile}
}


pack_info()
{
    slack_archive_names="$(pack_name_file)"

    mkdir -p ${T}
    for entry in ${slack_archive_names} ; do
        tar tf ${PACKDIR}/${entry} > ${T}/${entry}.index   
        cat ${T}/${entry}.index
    done                                 
}

pack_get_version()
{
    archive="$1"

    # remove leading path
    template_name="${archive##*/}"
    # remove file extension
    template_name="${template_name%.txz}"
    # get inner version
    inner_version="${template_name##*-}"
    # remove inner version
    template_name="${template_name%-*}"
    # remove arch
    template_name="${template_name%-*}"
    # detect version
    version="${template_name##*-}"

    if test -z "${version}" ; then
        awarn "Incorrect version in file ${archive}"
        return
    fi

    # do not forget inner version
    printf "%s\n" "${version}-${inner_version}"

}

pack_get_inner_version()
{
    archive="$1"

    filearch="${archive##*/}"
    version="${filearch##*-}"
    version="${version%.txz}"
    printf "%s" "${version}"
}

pack_define_by_filename()
{
    database=
    if test -d "/var/lib/pkgtools/packages/" ; then
        database='/var/lib/pkgtools/packages/'
    elif test -d "/var/log/packages/" ; then
        database='/var/log/packages/'
    fi
    while IFS='' read file_to_detect <&0 ; do
        if test -d "${file_to_detect}" ; then
            continue
        fi
        template="${file_to_detect#/}"
        grep -l -r -e "^${template}\$" ${database} 2> /dev/null || nonfail
    done
}


pack_dump_content_archive()
{
    archive="$1"
    dumppath="$2"

    archivename="${archive##*/}"
    tar tf "${archive}" > "${dumppath}/${archivename}.txt"
}
