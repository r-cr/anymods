slack_arch()
{
    if test -n "${any_packslack_arch}" ; then
        needed_arch="${any_packslack_arch}"
    else
        needed_arch="${ANYARCH}"
    fi

    case "${needed_arch}" in
        all)
            needed_arch='noarch'
        ;;
        amd64)
            needed_arch='x86_64'
        ;;
        x86)
            needed_arch='i586'
        ;;
    esac

    printf "%s" "${needed_arch}"
}

slack_format_description()
{
    packname="$1"
    if test -t 0 ; then
        die "slack_format_description reads from stdin"
    fi
    if test -z "${packname}" ; then
        packname="${PN}"
    fi
    whole_desc=
    desc_prefix="${packname}:"

    # read header and miss empty separator string
    read first_string <&0
    if test -z "${first_string}" ; then
        return 0
    fi
    first_string="${desc_prefix} ${first_string}"
    read empty_string <&0 || return 0
    whole_desc="${first_string}
${desc_prefix}"
    printf "%s\n" "${whole_desc}"

    # read exactly remaining 9 strings
    count=0
    while IFS='' read input_string <&0 ; do
        if test ${count} -ge 9 ; then
            awarn "Text for Slack description was truncated.
It must fit strictly into 2 header + 9 lines.
"
            break
        fi
        space_insert=
        if test -n "${input_string}" ; then
            space_insert=" "
        fi

        printf "%s\n" "${desc_prefix}${space_insert}${input_string}"

        count=$((count + 1))
    done

    # fill out the remaining strings, if there are some
    while test ${count} -lt 9 ; do
        printf "%s\n" "${desc_prefix}"
        count=$((count + 1))
    done

    # the result looks something like this:
#util: util - tool to do something important
#util:
#util: util is actively used with development and administration.
#util:
#util: It provides high compression rate, reusage of code and
#util: internal blessings.
#util:
#util:
#util:
#util:
#util:

}

slack_string_inner_version()
{
    if [ -n "${SLACK_INNER_VERSION}" ] ; then
        inner_candidate="${SLACK_INNER_VERSION}"
    else
        inner_num=$(fetch_inner_version)
        # release tag can't be alone without inner number, so expand it
        if [ -n "${RELEASE}" -a -z "${inner_num}" ] ; then
            inner_num=1
        fi
        if [ -n "${inner_num}" ] ; then
            inner_candidate="-${inner_num}"
        fi
        if [ -n "${RELEASE}" ] ; then
            inner_candidate="${inner_candidate}_${RELEASE}"
        fi
    fi

    if [ -n "${inner_candidate}" ] ; then
        printf "%s" "${inner_candidate}"
    fi
}
