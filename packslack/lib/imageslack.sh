imageslack_doc()
{
    actual_docfiles=
    if test -z "${DOCFILES}" ; then
        DOCFILES="AUTHORS COPYING* INSTALL MAINT NEWS README* TODO"
    fi
    for i in ${DOCFILES} ; do
        if test -f "${S}/$i" ; then
            actual_docfiles="${actual_docfiles} ${S}/$i"
        fi
    done
    if test -n "${actual_docfiles}" ; then
        mkdir -p ${D}/usr/doc/${P}/
        cp -pRf ${actual_docfiles} ${D}/usr/doc/${P}/
    fi
}

