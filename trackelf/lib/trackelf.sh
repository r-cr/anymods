READELF="$(${CC} -print-prog-name=readelf)"

trackelf_garbage()
{
    # more garbage strings to add here
    grep \
        -e "${BASEDIR}${any_write_root}" \
        - <&0 || nonfail
}

trackelf_rpath()
{
    if test -z "${any_trackelf_rpath}" ; then
        return 2
    fi
    rpath="$( printf "%s\n" "$1" | sed -n -e 's/RPATH:[ \t]*//p' )" || nonfail
    (
    IFS=':'
    rpath="${rpath}"
    for checked_one in ${rpath} ; do
        bad_flag='1'
        for allowed_one in ${any_trackelf_rpath} ; do
            if test "${checked_one}" = "${allowed_one}" ; then
                bad_flag=
                break
            fi
        done
        if test -n "${bad_flag}" ; then
            printf "%s\n" "Problems inside RPATH field"
            printf "%s\n" "Found on entry ${checked_one}"
        fi
    done
    )
}

trackelf_machine()
{
    if test -z "${any_trackelf_machine_type}" ; then
        return 2
    fi
    machine_type="$( printf "%s\n" "$1" | sed -n -e 's/Machine:[ \t]*//p' )" || nonfail
    if test "${machine_type}" != "${any_trackelf_machine_type}" ; then
        printf "%s\n" "Wrong machine type: ${machine_type}"
    fi
}

trackelf_filters_map()
{
    checked_info="$1"

    printf "%s\n" "${checked_info}" | trackelf_garbage || nonfail

    trackelf_rpath "${checked_info}" || nonfail
    trackelf_machine "${checked_info}" || nonfail
}

trackelf_checker_map()
{
    binfiles -d ${D} | while read file_to_check ; do

        diagnostic_output=
        if test -f "${file_to_check}" ; then

            elfinfo="$( ${READELF} -a ${file_to_check} )" || nonfail
            diagnostic_output="$( trackelf_filters_map "${elfinfo}" )" || nonfail
            if test -n "${diagnostic_output}" ; then
                printf "%s\n%s\n" "${file_to_check}" "${diagnostic_output}" 1>&2
            fi
        fi
    done
}
