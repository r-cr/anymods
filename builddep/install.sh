#!/bin/sh

set -o errexit

if [ ! -d "${ADIR}" ]  ; then
    printf "%s\n" "Variable ADIR with local working copy is not set. Nowhere to install shell code."
    exit 1
fi

if [ ! -d "${ADIR}/any/lib" ]  ; then
    printf "%s\n" "Directory ${ADIR}/any/lib is absent. Nowhere to install shell code."
    exit 1
fi

if [ ! -d "${ADIR}/any/libexec" ]  ; then
    printf "%s\n" "Directory ${ADIR}/any/libexec is absent. Nowhere to install executable script."
    exit 2
fi

set -o xtrace

cp -Rf lib/* ${ADIR}/any/lib
cp -Rf libexec/* ${ADIR}/any/libexec/

#cp -f README-any-* ${ADIR}/any/doc
