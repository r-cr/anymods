
builddep_uniqfiles="${any_rdd_root}/var/dump/builddep/${P}/file_usage.log"
builddep_image_pack_used="${T}/builddep/used_image_packs"
builddep_host_pack_used="${T}/builddep/used_host_packs"

builddep_local_root_define_pack()
{
    all_packs="$1"

    while read checked_file <&0 ; do
        if [ ! -f "${checked_file}" ] ; then
            continue
        fi
        ending_file="$(printf "%s\n" "${checked_file}" | \
            sed \
                -e "s#${ROOT}/##" \
                -e 's#/.*##'
            )"
        found_pack=
        for pack in ${all_packs} ; do
            found_file="$(find "${IMGDIR}/${pack}" -name "${IMGDIR}/${pack}/${ending_file}")"
            if [ -n "${found_file}" ] ; then
                printf "%s\n" "${pack}"
                break
            fi
        done
    done
}

builddep_define_pack_of_used_file()
{
    mkdir -p "${T}/builddep"
    : > ${builddep_image_pack_used}
    grep "${IMGDIR}" ${builddep_uniqfiles} | \
        sed \
        -e "s#${IMGDIR}/##" -e 's#/.*##' | \
        sort | uniq >> ${builddep_image_pack_used}

    : > ${builddep_host_pack_used}
    if [ -n "${any_builddep_host}" ] ; then
        grep -v "${IMGDIR}" ${builddep_uniqfiles} | \
            pack_define_by_filename | \
            sort | uniq > ${builddep_host_pack_used}
    fi

#    installed_packs="$(root_get_installed)" || nonfail
#    grep "${ROOT}" ${builddep_uniqfiles} | \
#        builddep_local_root_define_pack "${installed_packs}" \
#        >> ${builddep_image_pack_used}
}

builddep_output()
{
    if test -s "${builddep_image_pack_used}" ; then
        printf "%s\n\n" "Found build dependencies from the image ${P}:"
        cat ${builddep_image_pack_used}
    fi

    if test -s "${builddep_host_pack_used}" ; then
        printf "%s\n\n" "Found host dependencies for ${P}:"
        cat ${builddep_host_pack_used}
    fi
}

map()
{
    builddep_define_pack_of_used_file
    builddep_output
}
