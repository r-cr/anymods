#!/bin/sh

INSTALL="${INSTALL:-"cp -Rf"}"
MKDIR="${MKDIR:-"mkdir -p"}"
DESTDIR="${DESTDIR:-${ADIR}/rrr}"
PREFIX="${PREFIX:-}"
P="${P:-"anymods"}"
DOCDIR="${DOCDIR:-${PREFIX}/share/doc/${P}}"
MANDIR="${MANDIR:-${PREFIX}/share/man}"

set -o errexit

if [ ! -d "${ADIR}" ]  ; then
    printf "%s\n" "Variable ADIR with local working copy is not set. Nowhere to install shell code."
    exit 1
fi

if [ ! -d "${ADIR}/any/lib" ]  ; then
    printf "%s\n" "Directory ${ADIR}/any/lib is absent. Nowhere to install shell code."
    exit 1
fi

set -o xtrace

${INSTALL} lib/* ${ADIR}/any/lib

${MKDIR} ${DESTDIR}${DOCDIR}
${INSTALL} \
    README-any-* \
    "${DESTDIR}${DOCDIR}/"
