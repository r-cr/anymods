fetchdevgit_get()
{
    if [ "${any_fetch_patchgen_withstatic}" != '1' ] && \
        [ -d "${static_PATCHDIR}" -o -d "${atom_static_PATCHDIR}" ] ; then
        # do not work with devsrc, while there are patches prepared already
        return 0
    fi

    if [ "${any_remote_src_hashing}" = '1' ] ; then
        remote_name=$( hash_atom_name ${MY_PD} )
    else
        remote_name=${MY_PD}
    fi
    if [ "${any_local_src_hashing}" = '1' ] ; then
        local_name=$( hash_atom_name ${P} )
    else
        local_name=${P}
    fi
    local_dir=${ANYDEVSRCDIR}
    mkdir -p ${local_dir}

    if [ ! -d ${local_dir}/${local_name} -a -n "${any_fetch_dev_git_string}" ] ; then
        git clone ${any_fetch_dev_git_string}${remote_name} ${local_dir}/${local_name} 2> /dev/null || nonfail
    fi
    if [ "${any_fetch_dev_autoupdate}" != '0' -a -d "${local_dir}/${local_name}/.git" ] ; then
        (
        fetch_done=
        cd ${local_dir}/${local_name}

        git fetch || fetch_done='0'
        if [ "${fetch_done}" = '0' ] ; then
            awarn "\
No connection with origin repo of git sources was established.
It can be network problem or local sources have no origin source at all."
            return 0
        fi

        all_versions="${any_dev_src_version}"
        if [ -z "${all_versions}" ] ; then
            all_versions='master'
        fi
        current_branch="$(git symbolic-ref HEAD 2> /dev/null | sed -e 's#refs/heads/##')"
        local_branches="$(git branch | grep -v -e '*' || nonfail)"
        all_branches="$(git branch -r | grep -v -e 'HEAD' || nonfail)"
        all_tags="$(git tag -l)"

        for single_version in ${all_versions} ; do
            if [ "${current_branch}" = "${single_version}" ] ; then
                git pull origin ${single_version} || nonfail
                break
            fi
            tag="$(printf "%s\n" "${all_tags}" | grep -e "^${single_version}\$" )" || nonfail
            if [ -n "${tag}" ] ; then
                git checkout ${single_version}
                break
            fi
            local_branch="$(printf "%s\n" "${local_branches}" | grep -e " ${single_version}\$")" || nonfail
            if [ -n "${local_branch}" ] ; then
                git checkout ${single_version}
                git pull origin ${single_version} || nonfail
                break
            fi
            remote_branch="$(printf "%s\n" "${all_branches}" | grep -e "origin/${single_version}\$"  )" || nonfail
            if [ -n "${remote_branch}" ] ; then
                git checkout "origin/${single_version}" && \
                git checkout -b "${single_version}"
                git pull origin ${single_version}
                break
            fi
        done

        )
    fi
}

fetch_dev_inner_version()
{
    mkdir -p ${T}
    : > ${T}/src_commit_count
    : > ${T}/build_commit_count
    : > ${T}/patches_commit_count
    if [ "${any_local_src_hashing}" = '1' ] ; then
        local_name=$( hash_atom_name ${P} )
    else
        local_name=${P}
    fi
    local_dir=${ANYDEVSRCDIR}
    if [ -d "${local_dir}/${local_name}" ] ; then
        (
        cd ${local_dir}/${local_name}
        git log --no-merges --pretty=oneline . | wc -l > ${T}/src_commit_count
        )
    fi

    if [ -d "${static_PATCHDIR}" ] ; then
        (
        cd "${static_PATCHDIR}"
        git log --no-merges --pretty=oneline . | wc -l > ${T}/patches_commit_count
        )
    fi

    (
    cd ${DISTDIR}
    git log --no-merges --pretty=oneline . | wc -l > ${T}/build_commit_count
    )

    build_count="$(cat ${T}/build_commit_count 2> /dev/null)"
    src_count="$(cat ${T}/src_commit_count 2> /dev/null)"
    patches_count="$(cat ${T}/patches_commit_count 2> /dev/null)"

    printf "%s\n" $(( ${build_count:-0} + ${src_count:-0} + ${patches_count:-0} )) > ${T}/inner_version
}

fetch_dev()
{
    fetchdevgit_get
    fetch_dev_inner_version
}

fetch_gen_patches()
{
    # if ANYDEVSRCDIR points to PATCHDIR, it is interpreted as patches are already stored
    # in ANYDEVSRCDIR, so generation does not make sense
    if test -d "${ANYDEVSRCDIR}/${P}" && \
       test "${ANYDEVSRCDIR}/${P}" != "${PATCHDIR}" && \
       test  "${any_fetch_patchgen}" = '1' ; then

        if [ "${any_fetch_patchgen_withstatic}" = '1' ] || \
                [ ! -d "${static_PATCHDIR}" -a ! -d "${atom_static_PATCHDIR}" ] ; then
            (

            cd ${ANYDEVSRCDIR}/${P}
            genpatch-git ${S}
            rm -rf ${PATCHDIR}
            if test -n "$(ls -q ./genpatch/ 2>/dev/null )" ; then
                mkdir -p ${PATCHDIR}
                cp -pRf ./genpatch/* ${PATCHDIR}/
                rm -rf ./genpatch
            fi
            )
        fi
    fi

    fetch_merge_patchdir
}
