#!/bin/sh

set -o errexit

if test ! -d "${ADIR}" ; then
    printf "%s\n" "Variable ADIR with local working copy is not set. Nowhere to install shell code."
    exit 1
fi

if test ! -d "${ADIR}/any/lib" ; then
    printf "%s\n" "Directory ${ADIR}/any/lib is absent. Nowhere to install shell code."
    exit 1
fi

set -o xtrace

cp -Rf lib/* "${ADIR}/any/lib"
#cp -f README-any-* ${ADIR}/any/doc
mkdir -p "${ADIR}/rdd.conf.d/"
cp -f packdeb.conf "${ADIR}/rdd.conf.d/"
