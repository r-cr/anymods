admin_host()
{
    if [ "${any_host_update}" != '1' ] ; then
        return 0
    fi
    if [ "${bool_SANDBOX}" != '1' ] ; then
        return 0
    fi

    printf "%s\n" "Checking host environment..."
    missing_host_packs="$(depend_resolve_host)"
    if [ -z "${missing_host_packs}" ] ; then
        return 0
    fi
    printf "%s\n" "Updating host environment: ${missing_host_packs}"
    apt-get update
    apt-get install -fy --force-yes --reinstall ${missing_host_packs}
    missing_host_packs="$(depend_resolve_host)"
    if [ -n "${missing_host_packs}" ] ; then
        die "Host repository doesn't contain sufficient packages or their versions:
${missing_host_packs}
"
    fi
}
