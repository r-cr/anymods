
depend_get_single_host()
{
    single=$1
    (
    load_env ${single}
    for host_entry in ${HDEPEND} ; do
        deb_name_inside_package "${host_entry}"
        printf "\n"
    done
    ) || nonfail
}

depend_list_host()
{
    mkdir -p ${T}
    host_current="${T}/host_current"
    : > "${host_current}"
    dpkg -l | \
        sed \
            -e '1,5d' \
            -e 's/^\([^ ]*\) [ ]*\([^ ]*\) [ ]*\([^ ]*\)[ ].*/\2-\3/' \
        >> "${host_current}"

    printf "%s" "${host_current}"
}

depend_get_host_content()
{
    dpkg -l | \
        sed \
            -e '1,5d' \
            -e 's/^\([^ ]*\) [ ]*\([^ ]*\) [ ]*\([^ ]*\)[ ].*/\2-\3/'
}
