deb_arch()
{
    if test -n "${any_packdeb_arch}" ; then
        printf "%s" "${any_packdeb_arch}"
    else
        printf "%s" "${ANYARCH}"
    fi
}

deb_description()
{
    if [ -n "${DEB_DESCRIPTION}" ] ; then
        printf "%s" "${DEB_DESCRIPTION}"
    else
        printf "%s" "---"
    fi
}

deb_depend_boundary()
{
    boundary="$1"

    if [ "${boundary}" = '>' ] ; then
        printf "%s" '>>'
    elif [ "${boundary}" = '<' ] ; then
        printf "%s" '<<'
    elif [ -z "${boundary}" ] ; then
        printf "%s" '>='
    else
        printf "%s" "${boundary}"
    fi
}

deb_depends_format()
{
    result=''
    entered=''

    while read raw_entry <& 0 ; do
        if test -n "${entered}" ; then
            result="${result},"
        fi
        result="${result} ${raw_entry}"
        entered='1'
    done

    printf "%s" "${result}"
}

deb_depends()
{
    while read entry <& 0 ; do
        result=

        modificator=$(depend_detect_version_boundary $entry)
        entry=${entry#${modificator}}
        entry_name=$(depend_get_pack_name $entry)
        entry_deb_name=$(deb_name_inside_package ${entry_name})
        result="${entry_deb_name}"

        if [ "${entry_name}" != "${entry}" ] ; then
            entry_version=$(depend_get_pack_version $entry)
            deb_modificator="$(deb_depend_boundary ${modificator})"
            result="${result} (${deb_modificator} ${entry_version})"
        fi
        printf "%s\n" "${result}"
    done | deb_depends_format
}

deb_string_inner_version()
{
    inner_num=
    inner_candidate=$(fetch_inner_version)
    if test -n "${inner_candidate}" ; then
        update_string="${any_packdeb_update_style}"
        if test -z "${update_string}" ; then
            update_string='u'
        fi
        inner_num="${update_string}${inner_candidate}"
    fi
    update_connect=
    if test -n "${RELEASE}" -a -n "${inner_num}" ; then
        update_connect="${any_packdeb_update_style_connect}"
        if test -z "${update_connect}" ; then
            update_connect='.'
        fi
    fi
    if test -n "${RELEASE}" -o -n "${inner_num}" ; then
        printf "%s" "-${RELEASE}${update_connect}${inner_num}"
    fi

}

deb_name_inside_package()
{
    string=$1

    printf "%s" "${string}" | tr '[:upper:]' '[:lower:]' | tr '_' '-'
}

deb_sum_pack()
{
    if [ -n "${any_deb_packdir}" ] ; then
        PACKDIR=${any_deb_packdir}
    fi

    while read name <&0 ; do
        local_pack_names="$(printf "%s\n" "${name}" | pkgbase)"
        local_pack_files="$(printf "%s\n" "${name}" | pkgbin)"
        for packfile in ${local_pack_files} ; do
            if [ -f ${PACKDIR}/${packfile} ] ; then
                name_without_version="$(get_first_string "${local_pack_names}")"
                local_pack_names="$(delete_first_string "${local_pack_names}" "${name_without_version}" )"
                entry_name=$(deb_name_inside_package ${name_without_version})
                printf "%s\n" "${entry_name}"
            fi
        done
    done | deb_depends_format
}

deb_control_field_from_file()
{
    source_file="$1"
    if ! test -s "${source_file}" -a -f "${source_file}" ; then
        return
    fi

    result=''

    while IFS='' read raw_entry ; do
        for word_entry in ${raw_entry} ; do
            result="${result} ${word_entry}"
        done
    done < "${source_file}"
    result="${result# }"

    if test -n "${result}" ; then
        printf "%s" "${result}"
    fi
}

# single-line values only
deb_value_from_control()
{
    source_file="${1}"
    key="$2"

    if test ! -f "${source_file}" ; then
        return 1
    fi

    while IFS='' read line ; do
        case "${line}" in
            "${key}: "*)
                value="${line#${key}: }"
                printf "%s\n" "${value}"
            ;;
        esac
    done < "${source_file}"
}
