trackcontent_generate()
{
    rm -rf ${T}/trackcontent/ || nonfail
    mkdir -p ${T}/trackcontent/
    generate_pkg_listing > ${T}/trackcontent/files
}

trackcontent_store_map()
{
    trackcontent_generate
    sane_module_store "trackcontent"
}

trackcontent_checker_map()
{
    trackcontent_generate
    sane_module_compare "trackcontent"
}
