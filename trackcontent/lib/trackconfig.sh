trackconfig_generate()
{
    rm -rf ${T}/trackconfig || nonfail
    mkdir -p ${T}/trackconfig
    find ${S} -name "config.h" -type f | while read file_to_check ; do
       file_to_store="${file_to_check#${S}}"
       file_to_store="${T}/trackconfig/${file_to_store}.symbols"
       mkdir -p $(dirname ${file_to_store})

       cp -pf ${file_to_check} ${file_to_store}
    done
}

trackconfig_store_map()
{
    trackconfig_generate
    sane_module_store "trackconfig"
}

trackconfig_checker_map()
{
    trackconfig_generate
    sane_module_compare "trackconfig"
}
