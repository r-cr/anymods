trackgarbage_search()
{
    fallback="$(pack_fallback_allowed)"
    for binpack in $(pack_name) ; do
        bindir="$(pack_dir_for_runtime_entry "${binpack}" "${fallback}" )"
        grep -r -I -n \
            -e "${any_write_root}/" \
            "${bindir}"
    done
}

trackgarbage_checker_map()
{
    trackgarbage_search
}
