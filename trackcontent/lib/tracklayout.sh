track_PKG_CONTENT="${T}/track/content"

tracklayout_RULES_DIR="${METADIR}/layout"
tracklayout_RULES_TABLE="${METADIR}/layout/tablerules"
tracklayout_DIFF="${T}/difflayout"

tracklayout_find_nontemplate()
{
    if [ ! -f "${tracklayout_RULES_TABLE}" ] ; then
        skip "No table with needed layout"
    fi

    mkdir -p "$(dirname ${tracklayout_DIFF})"
    mkdir -p "$(dirname ${track_PKG_CONTENT})"
    rm ${tracklayout_DIFF} 2> /dev/null || nonfail
    generate_pkg_listing > ${track_PKG_CONTENT}

    pattern_select=
    file_pattern_match=
    full_file_pattern_match=
    cat ${tracklayout_RULES_TABLE} | while read line ; do
        pattern_select="$( printf "%s\n" "${line}" | cut -d' ' -f1 )"
        file_pattern_match="$( printf "%s\n" "${line}" | cut -d' ' -f2 )"
        if [ -z "${pattern_select}" -o -z "${file_pattern_match}" ] ; then
            continue
        fi
        full_file_pattern_match=${tracklayout_RULES_DIR}/${file_pattern_match}
        if [ ! -f "${full_file_pattern_match}" ] ; then
            die "No rule file for string: ${line}"
        fi

        grep -e "${pattern_select}" ${track_PKG_CONTENT} | grep -v -f ${full_file_pattern_match} >> ${tracklayout_DIFF} || nonfail
    done
}

tracklayout_output()
{
    if test -s "${tracklayout_DIFF}" ; then
        awarn "${P} : Non-matched files found."
        cat "${tracklayout_DIFF}"
        awarn ""
        die
    fi
}

tracklayout_map()
{
    tracklayout_find_nontemplate
    tracklayout_output
}

tracklayout_checker_map()
{
    tracklayout_map
}
