tracksymbols_generate()
{
    rm -rf ${T}/tracksymbols || nonfail
    mkdir -p ${T}/tracksymbols
    fallback="$(pack_fallback_allowed)"
    for subpack in $(pack_name) ; do
        bindir="$(pack_dir_for_runtime_entry "${subpack}" "${fallback}" )"
        binfiles -d "${bindir}" | while read file_to_check ; do
            if test -f "${file_to_check}" ; then
                file_to_store="${file_to_check#${bindir}}"
                file_to_store="${T}/tracksymbols/${file_to_store}.symbols"
                mkdir -p $(dirname ${file_to_store})

                nm -f posix -D ${file_to_check} | cut -d' ' -f1-2 > ${file_to_store}
            fi
        done
    done
}

tracksymbols_store_map()
{
    tracksymbols_generate
    sane_module_store "tracksymbols"
}

tracksymbols_checker_map()
{
    tracksymbols_generate
    sane_module_compare "tracksymbols"
}
