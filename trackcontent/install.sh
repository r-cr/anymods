#!/bin/sh

set -o errexit

if [ ! -d "${ADIR}" ]  ; then
    printf "%s\n" "Variable ADIR with local working copy is not set. Nowhere to install shell code."
    exit 1
fi

if [ ! -d "${ADIR}/any/lib" ]  ; then
    printf "%s\n" "Directory ${ADIR}/any/lib is absent. Nowhere to install shell code."
    exit 1
fi

set -o xtrace

cp -Rf lib/* ${ADIR}/any/lib
#cp -f README-any-* ${ADIR}/any/doc
