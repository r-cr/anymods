#!/bin/sh

INSTALL="${INSTALL:-"cp -Rf"}"
MKDIR="${MKDIR:-"mkdir -p"}"
DESTDIR="${DESTDIR:-${HOME}/rrr}"
PREFIX="${PREFIX:-}"
P="${P:-"anymods"}"
DOCDIR="${DOCDIR:-${PREFIX}/share/doc/${P}}"
MANDIR="${MANDIR:-${PREFIX}/share/man}"

if [ -f conf/config.sh ] ; then
    . conf/config.sh
fi

install_doc()
{
    ${MKDIR} "${DESTDIR}${DOCDIR}/"
    ${INSTALL} \
        'README-anymods' \
        'License-ISC' \
        "${DESTDIR}${DOCDIR}/"

}

set -o errexit
set -o xtrace

if [ -z "$1" ] ; then
    args="$(find . -maxdepth 1 -name "[a-z]*" -type d)"
else
    args="$@"
fi

install_doc
if test -d "${ADIR}" ; then
    ADIR="$(cd "${ADIR}" ; pwd -P)"
else
    ADIR="${DESTDIR}"
fi

for dir in ${args} ; do
    (
    cd ${dir}
    if [ -x ./install.sh ] ; then
        ADIR="${ADIR}" DESTDIR="${DESTDIR}" ./install.sh "${@}"
    fi
    )
done
