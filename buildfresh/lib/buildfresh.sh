buildfresh_determine_diff()
{
    pack="$1"

    checked_dir=${PACKDIR}
    if [ -n "${any_buildfresh_packdir}" ] ; then
        checked_dir=${any_buildfresh_packdir}
    fi

    pack_file=${checked_dir}/${pack}
    if [ ! -f ${pack_file} ] ; then
        return 1
    fi

    version_source=$(fetch_inner_version)
    version_binary=$(pack_get_inner_version ${pack_file})

    if [ -n "${version_source}" -a -n "${version_binary}" ] ; then
        version_diff=$(( ${version_source} - ${version_binary} ))
        printf "%s" ${version_diff}
    fi
}

buildfresh_changed()
{
    pack_name_file_actual | while read pack ; do
        version_diff="$(buildfresh_determine_diff ${pack})" || nonfail
        version_diff="$(( ${version_diff} + 0 ))"
        if [ "${version_diff}" -gt 0 ] ; then
            printf "%s\n" "${P}"
            break
        fi
    done
}

buildfresh_oldsource()
{
    pack_name_file_actual | while read pack ; do
        version_diff="$(buildfresh_determine_diff ${pack})" || nonfail
        version_diff="$(( ${version_diff} + 0 ))"
        if [ "${version_diff}" -lt 0 ] ; then
            printf "%s\n" "${P}"
        fi
    done
}

buildfresh_absent_packs()
{
    checked_dir=${PACKDIR}
    if [ -n "${any_buildfresh_packdir}" ] ; then
        checked_dir=${any_buildfresh_packdir}
    fi

    pack_name_file_actual | while read single_file ; do
        if [ ! -f ${checked_dir}/${single_file} ] ; then
            printf "%s\n" "${P}"
            break
        fi
    done

}

buildfresh_nonversioned()
{
    pack_name_file_actual | while read pack ; do
        version_diff="$(buildfresh_determine_diff ${pack})" || nonfail
        if [ -z "${version_diff}" ] ; then
            printf "%s\n" "${P}"
        fi
    done
}

buildfresh_map()
{
    buildfresh_changed
    buildfresh_absent_packs
}
